#       22-DAYTIME-SERVER
#
#!/usr/bin/python3
#-*- coding: utf-8-*-
#
#  usage: python3 22-daytime-server.py
# --------------------------------
# Escola del treball de Barcelona
# ASX 2HSX Curs 2022-2023
# Febrer 2023
#
# Lucas Rodríguez. a190074lr
# 13/02/2023 
#---------------------------------

import sys,socket
from subprocess import Popen, PIPE

HOST = ''
PORT = 50001
s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
s.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)
s.bind((HOST,PORT))
s.listen(1)
conn, addr = s.accept()

cmd = [ "date" ]

pipeData = Popen(cmd, stdout=PIPE)

for line in pipeData.stdout:
    conn.send(line)
    
conn.close()
sys.exit(0)

