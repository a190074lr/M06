#                    HEAD CHOICES
#!/usr/bin/python3
#-*- coding: utf-8-*-
#
#   head [-n 5|10|15] [-f file]
#    10 lines, file o stdin
#
#  usage: python3 04-head-choices.py
# --------------------------------
# Escola del treball de Barcelona
# ASX 2HSX Curs 2022-2023
# Gener 2023
#
# Lucas Rodríguez. a190074lr
# 23/01/2023 
#---------------------------------
# Especificacions d'entrada:
#
# $ head.py -n 2 -f file.txt
# $ head.py < file.txt
# $ head.py -n 3
# $ head.py -f file.txt
#
# Tots els altres casos errors.
#

import sys, argparse

parser = argparse.ArgumentParser(\
	description="""Mostrar les N primeres línies""",\
	epilog="that's all folks")

parser.add_argument("-n","--nlin", type=int,\
	help="Número de línies", dest="nlin",\
	metavar="[5|10|15]", choices=[5,10,15], default=10)

parser.add_argument("-f","--fit", type=str,\
	help="Fitxer a processar", metavar= dest="fitxer")

args=parser.parse_args()

print(args)

# ------------------------------------------------------

fileIn=open(sys.argv[1],"r")
counter=0
MAXLIN=args.nlin
fileIn=sys.stdin

if len(sys.argv)==2:
    fileIn=open(sys.argv[1],"r")

counter=0

for line in fileIn:
    counter+=1
    print(line, end="")
    if counter==MAXLIN: break
fileIn.close()

exit(0)

