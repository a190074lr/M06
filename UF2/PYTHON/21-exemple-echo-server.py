#       21-EXEMPLE-ECHO-SERVER
#
#!/usr/bin/python3
#-*- coding: utf-8-*-
#
#  usage: python3 21-exemple-echo-server.py
# --------------------------------
# Escola del treball de Barcelona
# ASX 2HSX Curs 2022-2023
# Gener 2023
#
# Lucas Rodríguez. a190074lr
# 10/02/2023 
#---------------------------------

import sys,socket

HOST = ''
PORT = 50001
s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
s.bind((HOST,PORT))
s.listen(1)
conn, addr = s.accept()
print("Conn",type(conn),conn)
print("Connected by;",addr)
while True:
    data = conn.recv(1024)
    if not data: break
    conn.send(data)
conn.close()
sys.exit(0)

