#       25-CLIENT-ONE2ONE
#
#!/usr/bin/python3
#-*- coding: utf-8-*-
#
#  usage: python3 22-daytime-client.py
# --------------------------------
# Escola del treball de Barcelona
# ASX 2HSX Curs 2022-2023
# Febrer 2023
#
# Lucas Rodríguez. a190074lr
# 13/02/2023 
#---------------------------------

import sys,socket,argparse
from subprocess import Popen, PIPE

parser = argparse.ArgumentParser(description="Server one2one",\
        epilog="Joking")

parser.add_argument("-s","--server",type=str,default='')
parser.add_argument("-p","--port",type=int,default=50001)

args=parser.parse_args()

HOST = args.server
PORT = args.port

#----------------------------------

s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
s.connect((HOST,PORT))

command = "ps ax"
pipeData = Popen(command,shell=True,stdout=PIPE)
  
for line in pipeData.stdout:
    s.send(line)
    
s.close()
  
sys.exit(0)
