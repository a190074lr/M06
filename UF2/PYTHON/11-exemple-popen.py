#                    EXEMPLE POPEN
#!/usr/bin/python3
#-*- coding: utf-8-*-
#
#  usage: python3 11-exemple-popen.py ruta
# --------------------------------
# Escola del treball de Barcelona
# ASX 2HSX Curs 2022-2023
# Gener 2023
#
# Lucas Rodríguez. a190074lr
# 01/02/2023 
#---------------------------------
# Especificacions d'entrada:
#

import sys, argparse
from subprocess import Popen, PIPE

parser = argparse.ArgumentParser(description = \
        """Exemple popen""")

parser.add_argument("ruta",type = str,\
        help = "Directori a llistar")

args = parser.parse_args()

#---------------------------------

command = [ "ls", args.ruta ]

pipeData = Popen(command, stdout=PIPE)

for line in pipeData.stdout:
    print(line.decode("utf-8"),end="")

exit(0)
