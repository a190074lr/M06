#                    POPEN SQL
#!/usr/bin/python3
#-*- coding: utf-8-*-
#
#  usage: python3 12-popen-sql.py ruta
# --------------------------------
# Escola del treball de Barcelona
# ASX 2HSX Curs 2022-2023
# FEBRER 2023
#
# Lucas Rodríguez. a190074lr
# 03/02/2023 
#---------------------------------
# Especificacions d'entrada:
#

import sys, argparse
from subprocess import Popen, PIPE

command = "psql -qtA -F',' -h localhost -U postgres training -c 'select * from clientes;'"

pipeData = Popen(command, shell=True,stdout=PIPE)

for line in pipeData.stdout:
    print(line.decode("utf-8"), end="")

exit(0)
