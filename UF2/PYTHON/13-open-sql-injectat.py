#                    POPEN SQL
#!/usr/bin/python3
#-*- coding: utf-8-*-
#
#  usage: python3 12-popen-sql.py "consulta sql"
# --------------------------------
# Escola del treball de Barcelona
# ASX 2HSX Curs 2022-2023
# FEBRER 2023
#
# Lucas Rodríguez. a190074lr
# 03/02/2023 
#---------------------------------
# Especificacions d'entrada:
#

import sys, argparse
from subprocess import Popen, PIPE

parser = argparse.ArgumentParser(description = "Consulta SQL")

parser.add_argument("sqlStatment",type = str,\
         help = "Consulta sql")

args = parser.parse_args()

#---------------------------------

cmd = "psql -qtA -F',' -h localhost -U postgres training"

pipeData = Popen(cmd, shell = True, bufsize=0, universal_newlines=True, stdin=PIPE, stdout=PIPE, stderr=PIPE)
#pipeData.stdin.write("select * from oficinas;\n\q\n")
pipeData.stdin.write(args.sqlStatment+"\n\q\n")

for line in pipeData.stdout:
    print(line, end="")

exit(0)
