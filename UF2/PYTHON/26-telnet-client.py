#       26-TELNET-CLIENT.py
#
#!/usr/bin/python3
#-*- coding: utf-8-*-
#
#  usage: python3 26-telnet-client.py
# --------------------------------
# Escola del treball de Barcelona
# ASX 2HSX Curs 2022-2023
# Febrer 2023
#
# Lucas Rodríguez. a190074lr
# 22/02/2023 
#---------------------------------

import sys,socket,argparse
from subprocess import Popen, PIPE

parser = argparse.ArgumentParser(description="Server one2one",\
        epilog="Joking")

parser.add_argument("-s","--server",type=str,default='')
parser.add_argument("-p","--port",type=int,default=50001)

args=parser.parse_args()

HOST = args.server
PORT = args.port

#----------------------------------

s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
s.connect((HOST,PORT))

while True:
    cmd = input('COMMAND: ')
    if cmd == 'exit': break
    conn.send(cmd)
    
s.close()
  
sys.exit(0)
