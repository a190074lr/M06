#       22-DAYTIME-CLIENT
#
#!/usr/bin/python3
#-*- coding: utf-8-*-
#
#  usage: python3 22-daytime-client.py
# --------------------------------
# Escola del treball de Barcelona
# ASX 2HSX Curs 2022-2023
# Febrer 2023
#
# Lucas Rodríguez. a190074lr
# 13/02/2023 
#---------------------------------

import sys,socket,argparse
from subprocess import Popen, PIPE

argparse

parser = argparse.ArgumentParser(description="Server one2one",\
        epilog="Joking")

parser.add_argument("-p","--port",type=int,help="A quin port",dest="port")

parser.add_argument("-s","--server",help="A quin port",dest="server")

args=parser.parse_args()

#----------------------------------

import sys,socket

HOST = args.server
PORT = args.port
s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
s.connect((HOST,PORT))

while True:
    data = s.recv(1024)
    if not data: break
    print(repr(data))
    
s.close()
sys.exit(0)

