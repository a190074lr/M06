#       22-DAYTIME-SERVER
#
#!/usr/bin/python3
#-*- coding: utf-8-*-
#
#  usage: python3 22-daytime-server.py
# --------------------------------
# Escola del treball de Barcelona
# ASX 2HSX Curs 2022-2023
# Febrer 2023
#
# Lucas Rodríguez. a190074lr
# 13/02/2023 
#---------------------------------

import sys,socket,argparse
from subprocess import Popen, PIPE

argparse

parser = argparse.ArgumentParser(description="Server one2one",\
        epilog="Joking")

parser.add_argument("-p","--port", type=int,\
        help="A quin port",dest="port")

args=parser.parse_args()

HOST = ''
PORT = args.port
s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
s.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)
s.bind((HOST,PORT))
s.listen(1)

while True:
    conn, addr = s.accept()

    cmd = [ "date" ]

    pipeData = Popen(cmd, stdout=PIPE)

    for line in pipeData.stdout:
        conn.send(line)
        
    conn.close()

