#                    POPEN SQL
#!/usr/bin/python3
#-*- coding: utf-8-*-
#
#  usage: python3 14-popen-sql-multi.py -d database -c numclie[...]
# --------------------------------
# Escola del treball de Barcelona
# ASX 2HSX Curs 2022-2023
# FEBRER 2023
#
# Lucas Rodríguez. a190074lr
# 03/02/2023 
#---------------------------------
# Especificacions d'entrada:
#

import sys, argparse
from subprocess import Popen, PIPE

parser = argparse.ArgumentParser(description = "Consulta SQL")

parser.add_argument("-d",type = str,\
         help = "Consulta sql", dest="database", default="training")

parser.add_argument("-c",type = str,\
         help = "Consulta sql", dest="sqlStatment", action="append")

args = parser.parse_args()

#---------------------------------

cmd = "psql -qtA -F',' -h localhost -U postgres " + args.database

pipeData = Popen(cmd, shell = True, bufsize=0, universal_newlines=True, stdin=PIPE, stdout=PIPE, stderr=PIPE)
#pipeData.stdin.write("select * from oficinas;\n\q\n")
pipeData.stdin.write(args.sqlStatment+"\n\q\n")

for line in pipeData.stdout:
    print(line, end="")

exit(0)

#select * from clientes where num_clie=2111
