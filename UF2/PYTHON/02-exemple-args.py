#                    EXEMPLE ARGS
#!/usr/bin/python3
#-*- coding: utf-8-*-
#
#   head [file]
#
#  usage: python3 02-exemple-args.py
# --------------------------------
# Escola del treball de Barcelona
# ASX 2HSX Curs 2022-2023
# Gener 2023
#
# Lucas Rodríguez. a190074lr
# 23/01/2023 
#---------------------------------
# Especificacions d'entrada: 10 lines, file o stdin
#

import argparse
parser = argparse.ArgumentParser(\
	description="programa exemple arguments",\
	prog="02-arguments.py",\
	epilog="hasta luego lucas!")

parser.add_argument("-e","--edat", type=int,\
	dest="useredat", help="edat a processar",\
	metavar="edat")

parser.add_argument("-n","--nom", type=str,\
	help="nom de usuari")

args=parser.parse_args()

print(args)
print(args.useredat, args.nom)

