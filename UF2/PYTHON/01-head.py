#                    HEAD
#!/usr/bin/python3
#-*- coding: utf-8-*-
#
#   head [file]
#
#  usage: python3 01-head.py
# --------------------------------
# Escola del treball de Barcelona
# ASX 2HSX Curs 2022-2023
# Gener 2023
#
# Lucas Rodríguez. a190074lr
# 23/01/2023 
#---------------------------------
# Especificacions d'entrada: 10 lines, file o stdin
#

import sys
fileIn=open(sys.argv[1],"r")
counter=0
MAXLIN=10
fileIn=sys.stdin

if len(sys.argv)==2:
    fileIn=open(sys.argv[1],"r")

counter=0

for line in fileIn:
    counter+=1
    print(line, end="")
    if counter==MAXLIN: break
fileIn.close()

exit(0)
