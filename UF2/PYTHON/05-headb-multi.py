#                    HEAD MULTI
#!/usr/bin/python3
#-*- coding: utf-8-*-
#
#   head [-n 5|10|15] [-f file]
#    10 lines, file o stdin
#
#  usage: python3 05b-head-choices.py
# --------------------------------
# Escola del treball de Barcelona
# ASX 2HSX Curs 2022-2023
# Gener 2023
#
# Lucas Rodríguez. a190074lr
# 30/01/2023 
#---------------------------------
# Especificacions d'entrada:
#
# $ head.py -n 2 -f file.txt
# $ head.py < file.txt
# $ head.py -n 3
# $ head.py -f file.txt
#
# Tots els altres casos errors.
#

# /usr/bin/python3
#-*- coding: utf-8-*-
#
# head [-n 5|10|15] file[...]
#  10 lines, file o stdin
# -------------------------------------
# @ edt ASIX M06 Curs 2022-2023
# Gener 2023
# -------------------------------------

import sys, argparse

parser = argparse.ArgumentParser(description=\
        "Mostrar les N primereslínies",\
        epilog="Joking")

parser.add_argument("-n","--nlin",type=int,\
        help="Número de línies",dest="nlin",\
        metavar="5|10|15", choices=[5,10,15],default=10)

parser.add_argument("fileList",type=str,\
        help="fitxer a processar", metavar="file",\
        action="append")

parser.add_argument("-v", "--verbose",action="store_true",default=False)

args=parser.parse_args()

print(args)

# -------------------------------------------------------

MAXLIN=args.nlin

def headFile(fitxer):
  fileIn=open(fitxer,"r")
  counter=0
  for line in fileIn:
    counter+=1
    print(line, end='')
    if counter==MAXLIN: break
  fileIn.close()

if args.fileList:
  for fileName in args.fileList:
    if args.verbose: print("\n",fileName, 40*"-")
    headFile(fileName)

exit(0)




