#       21-EXEMPLE-ECHO-CLIENT
#
#!/usr/bin/python3
#-*- coding: utf-8-*-
#
#  usage: python3 21-exemple-echoclient.py
# --------------------------------
# Escola del treball de Barcelona
# ASX 2HSX Curs 2022-2023
# Gener 2023
#
# Lucas Rodríguez. a190074lr
# 10/02/2023 
#---------------------------------

import sys,socket

HOST = ''
PORT = 50001
s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
s.connect((HOST,PORT))
s.send(b'Hello, world')
data = s.recv(1024)
s.close()
print('Recived', repr(data))
sys.exit(0)

