#       EXEMPLE-EXECV                   
#!/usr/bin/python3
#-*- coding: utf-8-*-
#
#   head [-n 5|10|15] [-f file]
#    10 lines, file o stdin
#
#  usage: python3 19-exemple-execv.py
# --------------------------------
# Escola del treball de Barcelona
# ASX 2HSX Curs 2022-2023
# Gener 2023
#
# Lucas Rodríguez. a190074lr
# 10/02/2023 
#---------------------------------

import sys,os
print("Hola, començament del programa principal")
print("PID pare: ", os.getpid())

pid=os.fork()
if pid !=0:
  print("Programa pare ", os.getpid(), pid)
  sys.exit(0)

print("Programa Fill ", os.getpid(), pid )
#os.execv("/usr/bin/ls",["/usr/bin/ls","-la","/","/opt"])
#os.execl("/usr/bin","/usr/bin/ls","-lh","/opt")
#os.execlp("ls","ls","-lh","/opt")
#os.execvp("uname",["uname","-a"])
#os.execv("/bin/bash",["/bin/bash","show2.sh"])
os.execle("/bin/bash","/bin/bash","show.sh",{"nom":"joan","edat":"25"})

#aquest codi no s'executa mai
print("Hasta luego Lucas!")
sys.exit(0)
