#! /bin/bash

# Engegar serveis per tenir-ne els usuaris ldap.

/usr/sbin/nslcd
/usr/sbin/nscd

# Share public.

mkdir /var/lib/samba/public
chmod 777 /var/lib/samba/public
cp /opt/docker/* /var/lib/samba/public/.

# Share privat
mkdir /var/lib/samba/privat.

cp /opt/docker/*.md /var/lib/samba/privat/.

# Executem el script "add_ldapusers_samba.sh" que afegirà tots els usuaris de LDAP al servei de SAMBA.

bash ./add_ldapusers_samba.sh
pdbedit -L

# Copiem la configuració de samba personal al docker.

cp /opt/docker/smb.conf /etc/samba/smb.conf

# Activem els serveis.

/usr/sbin/smbd && echo "smb Ok"
/usr/sbin/nmbd -F && echo "nmb  Ok"
