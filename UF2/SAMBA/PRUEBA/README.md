# SAMBA22:HOME_SERVER
# Lucas Rodríguez Cabañeros a190074lr

## EDICIÓ DE FITXERS

__Editar el fitxer /etc/ldap/ldap.conf__.

> ```
> BASE	dc=edt,dc=org
> URI	ldap://ldap.edt.org
> ```

__Editar el fitxer /etc/hosts__.

> ```
> 172.19.0.2	ldap.edt.org
> 172.18.0.3    smb.edt.org
> ```

__Editar el fitxer /etc/nsswicht.conf__.

> ```
> passwd:         files ldap
> group:          files ldap
> ```

__Editar el fitxer /etc/nslcd.conf__.

> ```
> uri ldap://ldap.edt.org
> base dc=edt,dc=org
> ```

__Editar el fitxer /etc/pam.d/common-session-> Per la creació del home dir.__

> ```
> session	required	pam_unix.so
> session   optional	pam_mkhomedir.so
> session	optional	pam_mount.so
> session	[success=ok default=ignore]	pam_ldap.so minimum_uid=1000
> ```

__Editar el /etc/security/pam-mount.conf.xml per afegir el directori tmp.__


> ```
> <volume
>	user="*"
>	fstype="tmpfs"
>	options="size=100M"
>	mountpoint="~/tmp"
> />
> ```

__Editar el /etc/samba/smb.conf per afegir els directoris dels usuaris de LDAP.__


> ```
> # -------------------------------------------
> # Homes LDAP
> # -------------------------------------------
> [LDAPhomes]
>	comment = Directori de homes LDAP
>	path=/tmp/home
>	public = yes
>	browseable = yes
>	writable = yes
> ```

## CREACIO DE SCRIP LDAPUSERS

> ```
> #! /bin/bash
>
> # Variable que contindrà 
>
> llistaUsers=$(ldapsearch -x -LLL -h ldap.edt.org -b 'ou=usuaris,dc=edt,dc=org' uid| grep ^uid | cut -d: -f2)
> for user in $llistaUsers
> do
>  echo -e "$user\n$user" | smbpasswd -a $user
>  line=$(getent passwd $user)
>  uid=$(echo $line | cut -d: -f3)
>  gid=$(echo $line | cut -d: -f4)
>  homedir=$(echo $line | cut -d: -f6)
>  echo "$user $uid $gid $homedir"
>  if [ ! -d $homedir ]; then
>    mkdir -p $homedir
>    cp -ra /etc/skel/. $homedir
>    chown -R $uid.$gid $homedir
>  fi
> done
> ```

## CREACIO DE SCRIP STARTUP

> ```
> #! /bin/bash
>
> # Engegar serveis per tenir-ne els usuaris ldap.
>
> /usr/sbin/nslcd
>/usr/sbin/nscd
>¡
> # Share public.
>
> mkdir /var/lib/samba/public
> chmod 777 /var/lib/samba/public
> cp /opt/docker/* /var/lib/samba/public/.
>
> # Share privat
> mkdir /var/lib/samba/privat.
>
> cp /opt/docker/*.md /var/lib/samba/privat/.
>
> # Executem el script "add_ldapusers_samba.sh" que afegirà tots els usuaris de LDAP al servei de SAMBA.
>
> bash ./add_ldapusers_samba.sh
> pdbedit -L
>
> # Copiem la configuració de samba personal al docker.
>
> cp /opt/docker/smb.conf /etc/samba/smb.conf
>
> # Activem els serveis.
>
> /usr/sbin/smbd && echo "smb Ok"
> /usr/sbin/nmbd -F && echo "nmb  Ok"
> ```

## CREAR IMATGE

```
$ docker build -t a190074lr/samba22:home_server .
```

## ENGEGAR CONTAINERS

```
$ docker run --rm --name ldap.edt.org -h ldap.edt.org --net 2hisx -p 389:389 -d a190074lr/ldap22:latest
```
```
$ docker run --rm --name smb.edt.org -h smb.edt.prg --net 2hisx --privileged -d a190074lr/samba22:home_server
```

## COMPROVACIONS

Comprovar connectivitat entre ldap container i pam container.

· Entrem de manera interactiva al container pam.

> ```
> $ docker exec -it smb.edt.org /bin/bash
> ```

> ```
> $ nmap ldap.edt.org
>
> Starting Nmap 7.80 ( https://nmap.org ) at 2023-01-27 10:03 UTC
> Nmap scan report for ldap.edt.org (172.19.0.2)
> Host is up (0.000017s latency).
> rDNS record for 172.19.0.2: ldap.edt.org.2hisx
> Not shown: 999 closed ports
> PORT    STATE SERVICE
> 389/tcp open  ldap
> MAC Address: 02:42:AC:13:00:02 (Unknown)
>
> Nmap done: 1 IP address (1 host up) scanned in 0.19 seconds
> ```

 · $ getent passwd.

 · Getent passwd usuari-ldap.

> ```
> $ getent passwd pere
> ```

 · Iniciar sessió usuari-ldap.

> ```
> $ su - pere
> ```

 · $ ip a $ pwd

 · Llistar els comptes d'usuaris samba(breu).

> ```
> $ pdbedit -L
> ```

 · Llistar tota l'informació del compte samba s'un usuari concret.

> ```
> $ pdbedit -u pere
> ```










































