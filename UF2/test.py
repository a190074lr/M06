#                    EXEMPLE POPEN
#!/usr/bin/python3
#-*- coding: utf-8-*-
#
#  usage: python3 11-exemple-popen.py ruta
# --------------------------------
# Escola del treball de Barcelona
# ASX 2HSX Curs 2022-2023
# Gener 2023

from subprocess import Popen, PIPE

cmd = [ "date" ]

pipeData = Popen(cmd, shell = True, bufsize=0, universal_newlines=True, stdin=PIPE, stdout=PIPE, stderr=PIPE)

for line in pipeData.stdout:
    data = (line)

print(data)

exit(0)
