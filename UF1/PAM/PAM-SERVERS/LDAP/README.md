#	AWS LDAP AUTENTICHATE

## INSTAL·LACIÓ D'UN MINILINUX ( ORDINADOR REAL )

Seguirem la guia d'instal·lació de l'escola:

> · https://gitlab.com/manelmellado/ubnt-at-inf

Informació a tenir en compte:

> · *Nova partició logial:* __2HISIX Minilinux -> /dev/sda2.__

> · *Partició swap:* __Deixar-la a /dev/sda7.__

> · *Treure l'opció d'instal·lar l'interfície gràfica:* __Desktop,GNOME.__

Retocar el grub:

> __Retocar els Labels.__

> ```
> $ vim /boot/grub/grub.cfg
> ```

> __Retocar el Timeout al fitxer `/etc/default/grub`__.

> ```
> GRUB_TIMEOUT=-1
> ```

## CREACIÓ DEL AWS

La creació de l'instància d'amazon la fem des de la mateixa pàgina.

> · https://us-east-1.console.aws.amazon.com/console/home?region=us-east-1

Procés __create instance__:

> · *Name:* __LDAP.__

> · *ISO Image:* __Debian.__

> · *Instance type:* __t2.micro.__

> · *Key par (login):* __prova-aws (utilitzar la JA existent).__

> · *Network settings:* __create security group.__

Procés __configure security groups__:

> · *Edit inbound rules:* __add rule -> tpye: LDAP. -> source: 0.0.0.0/0__

## CONFIGURACIÓ AWS

__1r.__ Instal·lar y configurar docker engine.

> · Seguir el passos de la web:

>> - https://docs.docker.com/engine/install/debian/

>> - https://docs.docker.com/engine/install/linux-postinstall/

## INICIALITZAR EN EL AWS EL LDAP:LATEST

Crear la docker network __2hisx__:

> ```
>  $ docker network create 2hisx
> ```

Inicialitzar el docker:

> ```
> $ docker run --rm --name ldap.edt.org -h ldap.edt.org --net 2hisx -p 389:389 -d a190074lr/ldap22:latest
> ```

## INSTALACIÓ DE PAQUETS NECESSARI ( ORDINADOR REAL )

```
$ apt-get -y install procps iproute2 tree nmap vim less finger passwd libpam-pwquality libpam-mount libnss-ldapd libpam-ldapd nslcd nslcd-utils ldap-utils
```

## EDICIÓ DE FITXERS ( ORDINADOR REAL )

__Editar el fitxer `/etc/ldap/ldap.conf`__.

> ```
> BASE	dc=edt,dc=org
> URI	ldap://ldap.edt.org
> ```

__Editar el fitxer `/etc/hosts`__.

> ```
> 54.164.136.205	ldap.edt.org
> ```

__Editar el fitxer `/etc/nsswicht.conf`__.

> ```
> passwd:         files ldap
> group:          files ldap
> ```

__Editar el fitxer `/etc/nslcd.conf`__.

> ```
> uri ldap://ldap.edt.org
> base dc=edt,dc=org
> ```

__Editar el fitxer `/etc/pam.d/common-session` -> Per la creació del home dir.__

> ```
> session	required	pam_unix.so 
> session optional	pam_mkhomedir.so
> session	optional	pam_mount.so 
> session	[success=ok default=ignore]	pam_ldap.so minimum_uid=1000
> ```

## REINICIAR ELS SERVIDORS ( ORDINADOR REAL )

__Reiniciar el servidor `nsswicht.service`__.

> ```
> $ systemctl restart nsswicht.service
> ```

__Reiniciar el servidor `nslcd.service`__.

> ```
> $ systemctl restart nscld.service
> ```

## CREACIÓ USUARIS UNIX ( ORDINADOR REAL )

Utilitzem el script per tal de crear-hi els usuaris unix, __SENSE LA CREACIÓ DE DIR HOME.__

> ```
> for user in unix01 unix02 unix03 unix04 unix05
> do
>	useradd -s /bin/bash $user
>	echo -e "$user\n$user" | passwd $user
> done
> ```

## COMPROVACIONS ( ORDINADOR REAL )

__1r.__ Comprovacions users ldap.

> ````
> $ login marta
> Password: 
> Linux g12 5.10.0-19-amd64 #1 SMP Debian 5.10.149-2 (2022-10-21) x86_64
>
> The programs included with the Debian GNU/Linux system are free software;
> the exact distribution terms for each program are described in the
> individual files in /usr/share/doc/*/copyright.
> 
> Debian GNU/Linux comes with ABSOLUTELY NO WARRANTY, to the extent
> permitted by applicable law.
> Creating directory '/tmp/home/marta'.
> ```

__2n.__ Comprovacions users unix.

> ```
> $ login unix02
> Password: 
> Linux g12 5.10.0-19-amd64 #1 SMP Debian 5.10.149-2 (2022-10-21) x86_64
>
> The programs included with the Debian GNU/Linux system are free software;
> the exact distribution terms for each program are described in the
> individual files in /usr/share/doc/*/copyright.
>
> Debian GNU/Linux comes with ABSOLUTELY NO WARRANTY, to the extent
> permitted by applicable law.
> Last login: Wed Nov 23 12:01:19 CET 2022 on pts/0
> ```

## CONTAINER PHPLDAPADMIN

Encendre el container.

> ```
> $ docker run --rm --name phpldapadmin.edt.org -h phpldapadmin.edt.org --net 2hisx -p 80:80 -d a190074lr/ldap22:phpldapadmin
> ```

## CONTAINER LDAP

Encendre el container.

> ```
> $ docker run --rm --name ldap.edt.org -h ldap.edt.org --net 2hisx -p 389:389 -d a190074lr/ldap22:latest
> ```

## CONTAINER PAM

Encendre el container.

> ```
> $ docker run --rm --name pam.edt.org -h pam.edt.org --net 2hisx --privileged -it a190074lr/pam22:ldap
> ``













