#	PAM BASE

## DOCKERFILE

Creem el Dockerfile del nostre pam base

```
FROM debian:latest
LABEL author="@edt ASIX Curs 2022"
LABEL subject="ldapserver 2022"
#ARG DEBIAN_FRONTEND=noninteractive
RUN apt-get update
RUN apt-get install -y procps iproute2 tree nmap vim less finger passwd libpam-pwquality libpam-mount
RUN mkdir /opt/docker/
WORKDIR /opt/docker/
COPY * /opt/docker/
RUN chmod +x /opt/docker/startup.sh
CMD /opt/docker/startup.sh
```

## STARTUP.SH

Editar el script per tal de crear nous usuaris dins el container.

```
for user in unix01 unix02 unix03 unix04 unix05
do
  useradd -m -s /bin/bash $user
  echo -e "$user\n$user" | passwd $user
done

/bin/bash
```

## POSAR EN MARXA EL CONTAINER

Construir l'imatge.

```
$ docker build -t a190074lr/pam22:base .
```

Encendre el container.

```
$ docker run --rm --name pam.edt.org -h pam.edt.org --net 2hisx -it a190074lr/pam22:base
```

## PAM_MOUNT

### 1. Editar el fitxer */etc/security/pam_mount.conf.xml*.

> __A tots els usuaris es munta dins el seu home un recurs anomenat tmp de 100M
corresponent a un ramdisk tmpfs.__

> ```
> root@pam:/opt/docker# su unix01
reenter password for pam_mount:
unix01@pam:/opt/docker$ cd
unix01@pam:~$ ls
tmp
unix01@pam:~$ mount -t 
.bash_history  .bash_logout   .bashrc        .profile       tmp/           
unix01@pam:~$ mount -t tmpfs
tmpfs on /dev type tmpfs (rw,nosuid,size=65536k,mode=755)
shm on /dev/shm type tmpfs (rw,nosuid,nodev,noexec,relatime,size=65536k)
none on /home/unix01/tmp type tmpfs (rw,relatime,size=102400k,gid=1000)
unix01@pam:~$ df -h
Filesystem      Size  Used Avail Use% Mounted on
overlay          89G   13G   72G  15% /
tmpfs            64M     0   64M   0% /dev
shm              64M     0   64M   0% /dev/shm
/dev/sda5        89G   13G   72G  15% /etc/hosts
none            100M     0  100M   0% /home/unix01/tmp
