# SSHFS

## EDITAR EL DOCKER FILE

Editar el fitxer *DockerFile*

__1r. Introduïr en el procés d'instal·lació els paquets *sshfs*.__

> ```
> RUN apt-get install -y sshfs
> ```

## EDITAR EL STARTUP

Editar el el script startup per tal de alhora que s'encengui el container amb les modificacions corresponents.

> ```
> mkdir /root/.ssh
> chmod 700 /root/.ssh
> #cp /opt/docker/known_hosts /root/.ssh/known_hosts
> ssh-keyscan ssh.edt.org >> /root/.ssh/known_hosts
>
> /bin/bash
> ```

>> __mkdir /root/.ssh__ -> *Ho fem per que alhora d'encendre el servei surt crei la carpeta de configuració de l'usuari el servei ssh.*
>> __chmod 700 /root/.ssh__ -> *Donar-li permissos.*
>> __ssh-keyscan ssh.edt.org >> /root/.ssh/known_hosts__ -> *Evitar-ne problemes amb el fingerprint.*

## CONSTRUIR L'IMATGE

> ```
> $ docker built -t a190074lr/ssh22:sshfs .
> ```

## INICIALITZAR EL CONTAINER SSHFS

> ```
> $ docker run --rm --name sshfs.edt.org --hostname sshfs.edt.org --net 2hisx --privileged --cap-add SYS_ADMIN --device /dev/fuse  --security-opt apparmor:unconfined -it a190074lr/ssh22:sshfs
> ```

## INICIALITZAR EL CONTAINER SSH

> ```
> $ docker run --rm --name ssh.edt.org -h ssh.edt.org --net 2hisx -d a190074lr/ssh:base
> ```

## INICIALITZAR EL CONTAINER LDAP

> ```
> $ docker run --rm --name ldap.edt.org -h ldap.edt.org --net 2hisx -d a190074lr/ldap22:latest
> ```