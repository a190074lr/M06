+-## APUNTS SSH

### ENLLAÇOS APUNTS

> *https://gitlab.com/edtasixm06/m06-aso/-/tree/main/UF1_3_acces_remot*

> *https://github.com/edtasixm01/LPIC-1/tree/main/110-Security/110.3-Securing_data_with_encyption*

### PASOS PER CONECTAR-SE PER SSH

1. Indentificar el host destí.

> *· Mostrar el fingerprint.*

> *· Confirmar-ho.*

>> *SHA256:lwrUNv0Mg5CIFszNnv6yKg9OY2FP0q1lAO0GkFvFn8U -> Hash en format __SSH__.*

2. Autenticar l'usuari.

### ORDRES CLIENT

1. Ordre per llistar els paquest de ssh.

> ```
> $ dpkg -l | grep ssh
> ```

2. Paquet com a client

> ```
> $ dpkg -L openssh-client | grep bin
> ```

>> *grep | bin -> Per veure els executables.*

3. Comprovar funcioment ssh.

> ```
> $ sudo systemctl status ssh
> ```

4. Veure el port que utilitza.

> ```
> $ nmap localhost
> ```

>> *Port ssh -> 22/tcp   open  ssh*

5. Cambiar de manera "manual" el fingerprint.

> ```
> $ ssh-keygen -f "/home/users/inf/hisx2/a190074lr/.ssh/known_hosts" -R "remot"
> ```

6. Com es diu la configuració clie.

> ```
> /etc/ssh/ssh_config
> ```

7. Com es diu la configuració servidor.

> ```
> /etc/ssh/sshd_config
> ```

9. Configuració personal de l'usuari.

> ```
> ssh/sshd_config
> ```

10. Admetre opcions de configuració client de ssh en la línia de comandes.

> ```
> $ ssh -o VisualHostKey=no guest@g12
> ```

11.Fer actiu el entorn gràfic.

> ```
> $ ssh -X guest@g12
> ```

> 11.1 Accedir al sistema de fitxers d'un ordinador remot (en aquest cas guest).

>> ```
>> $ nautilus --no-desktop --browser &
>> ```

12. Per copiar una clau des de el client al servidor.

>> ```
>> $ ssh-copy-id user01@g12
>> ```

13. El ps el fa al container i el wc al container

>> ```
>> $ ssh unix01@172.18.0.3 ps | wc -l
>> ```

14. Fa l'ordre ps ax en el container y conta les liníes i genera un fitxer DINS EL CONTAINER.

>> ```
>> $ ps ax | ssh unix01@172.18.0.3 ps "wc -l > /tmp/log"
>> ```

__· WILDCARD -> 0.0.0.0 -> Escolta a totes les interfícies.__

### ODRES D'OPCIONS DE CONNEXIONS

1. Opcions de la linia d'ordre.

2. Opcions personals de l'usuari.

> ```
> ssh/sshd_config
> ```

3. Opcions clients globals.

> ```
> /etc/ssh/ssh_config
> ```

### OPCIONS DE CONFIGURACIÓ DEL CLIENT (pg.4)

> ```
> $ sudo vim /etc/ssh/ssh_config
> ```

> #### OPCIONS DE HOST * > Contra quins host em conecto.

> · Amb quins servidors contactarà.

>> ```
>> PORT 22
>> ```

>>> __A tots aquells servidor que tinguin el port 2020.__

> · Si volem autentificació amb passwd.

>> ```
>> PasswordAuthentication=yes/no
>> ```

> · Si volem el fitxer .ssh/known_hosts encriptat.

>> ```
>> HashKnownHosts yes/no
>> ```

> · Per veure una "representació gràfica" de la __key__.

>> ```
>> VisualHostKey=yes/no
>> ```

### OPCIONS DE CONFIGURACIÓ DEL SERVIDOR

> ```
> $ sudo vim /etc/ssh/sshd_config
> ```

· Si es poden conectar autentificar-se com a root.

> ```
> PermitRootLogin prohibit-password
> ```

> #### MACHT

· Fitxer que utilitza les reglas de PAM al servei ssh.

> ```
> /etc/pam.d/sshd
> ```

kill -1 -> SIGHUP: Reinicia el servei.

ss = netstart, veure informació dels ports.

## ADMETRE CONNEXIÓ ROOT

Editar el fitxer __/etc/ssh/sshd_config__

> ```
> PermitRootLogin yes
> ```

> ```
> $ ssh -v root@localhost
> ```

>> __-v__: *Verbose de la procediment de connexió ssh*

>>> · __-vvv__: *Més v més informació durant el procés de connexió de ssh.*

## ADMETRE CONNEXIONS D'USUARIS ESPECÍFICS

Editar el fitxer __/etc/ssh/sshd_config__

> ```
> AllowUsers unix01
> ```

## NO ADMETRE AUTENTIFICACIÓ PER USUARI I PASSWD

Editar el fitxer /etc/ssh/sshd_config

> ```
> PasswordAuthentication no
> ```

## EXEMPLE MACHT

Editar el fitxer __/etc/ssh/sshd_config__

> ```
> Match user unix02
>   banner /etc/banner
> 
> Match user unix03
>   banner /etc/banner2
> ```

>> Editar el missatge que surt alhora de connectar-se de forma remota al usuari destí ssh unix02@172.18.0.3

> ```
> Match host 172.18.0.1
>   banner /etc/banner2
> ```
 
>> Editar el missatge que surt alhora de connectar-se de forma remota que provingui del host 172.18.0.1

> ```
> Match host *172.18.0.1*.escoladeltreball.org
>   banner /etc/banner2
> ```
 
>> Editar el missatge que surt alhora de connectar-se de forma remota que provingui del domini .escoladeltreball.org


## ORDRES VARIES

sshfs -> Sistema de fitxers basat en ssh.

> ```
> $ sshfs unix01@172.18.0.3: /tmp/mnt
> ```

>> __Monta el directori home de l'usuari unix01 al directori del local host */tmp/mnt*.__

Fer l'odre mount per comprovar que s'ha munta.

> ```
> $ mount -t fuse.sshfs
> ```

Desmuntar.

> ```
> $ fusermount -u /tmp/mnt
> ```

>> __No és pot estar dins del directori si l'anem a desmuntar.__



FUSER: File System In User Space
RDP: Remote Desktop Protocol

Activar un agent ssh

- $ eval "$(ssh-agent -s)"

    > Engega un dimoni que manté en memoria les associcaions de les claus privades amb las passphrase.

Associar la clau prova amb l'agent

- ssh-add .ssh/prova2

Llista las claus associades.

- ssh-add -l







