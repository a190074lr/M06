#	PAM/ SSH

## EDITAR EL DOCKER FILE

Editar el fitxer *DockerFile*

__1r. Introduïr en el procés d'instal·lació els paquets *openssh-server* y *openssh-client*.__

> ```
> RUN apt-get install -y openssh-server openssh-client
> ```

## EDITAR EL STARTUP

Editar el el script startup per tal de alhora que s'encengui el container inicialitzi el servei *ssh*__.

> ```
> mkdir /run/sshd
> /usr/sbin/sshd  -D
> ```

>> __mkdir__ -> *Ho fem per que alhora d'encendre el servei surt l'error de que hi falta aquest directori en concret, així que el creem.*

>> __-D__ -> *Per encentre el servei en detach.*

## CONSTRUIR L'IMATGE

> ```
> $ docker built -t a190074lr/ssh22:base .
> ```


## INICIALITZAR EL CONTAINER SSH

> ```
> $ docker run --rm --name ssh.edt.org -h ssh.edt.org --net 2hisx -d a190074lr/ssh:base
> ```

## INICIALITZAR EL CONTAINER LDAP

> ```
> $ docker run --rm --name ldap.edt.org -h ldap.edt.org --net 2hisx -d a190074lr/ldap22:latest
> ```

## COMPROVACIONS

__1r. Accedir al container de manera interactiva.__

> ```
> $ docker exec -it a190074lr/ssh22:base /bin/bash
> ```

__2n. Intentar contectarnos de forma remota a un usuari local i a un usuari de ldap.__

> __USUARI LOCAL:__

>> ```
>> # ssh unix01@localhost
>> ```

> __USUARI LDAP:__

>> ```
>> # ssh pere@localhost
>> ```