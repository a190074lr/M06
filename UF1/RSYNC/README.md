# PRÀCTICA RSYNC

## Using Rsync to Sync with a Remote System

### PUSH

“Pushes” a directory from the local system to a remote system.

> ```
> $ rsync -av dir1 a190074lr@10.200.245.217:/tmp
> ```

### PULL

Is used to sync a remote directory to the local system.

> ```
> $ rsync -av a190074lr@10.200.245.217:/tmp/remot1 .
> ```

## RSYNC SERVIDOR

· Generarem tres recursos rsync.

· Configurarem els recursos de manera que alguns siguin de 'rw' i altres de 'ro'.

· Configurarem l'accés a certs usuaris.

> __1. Generarem un arxiu en el servidor (g17) que es dirà '/etc/rsyncd.conf'__

>> ```
>> lock file = /var/run/rsync.lock
>> log file = /var/log/rsyncd.log
>> pid file = /var/run/rsyncd.pid
>>
>> [epoptes]
>>         path = /tmp/epoptes
>>         comment = Epoptes compartido
>>         uid = guest
>>         gid = guest
>>         read only = no
>>         list = yes
>>         auth users = clientersync,guest
>>         secrets file = /etc/rsyncd.secretos
>>         hosts allow = *
>> ```

> *__lock file__ -> L'arxiu que rsync utilitza per controlar el num màxim de connexions.*
>
> *__log file__ -> És on rysnc guarda un registre sobre la seva activitat; quan va començar a escoltar, quan i d'ón és connectan altres ordinadors, i qualsevolr error que es trobi.*
>
> *__pid file__ -> És ón el servei r sync escriura el ID del procés que se li ha assignat, aixó és  útil perquè podem utilitzar aquesta id del procés per parar el servei.*
>
> __PARÀMETRES GLOBAL__
>
>> *__[nom]__ -> És el nom que li assignem al módul. Cada módul exporta un arbre de directoris. El nom del módul no pot contenir diagonal o un corchet de tancament.*
>>*
>> *__path__ -> És la ruta a la carpeta que estem fent disponible amb rsync.*
>>
>> *__comment__ -> És un comentari que apareix conjuntament al nom del módul quan un client obté la llista de tots el móduls disponibles.*
>>
>> *__uid__ -> Quan el servei de rsync és executat com a guest, podem especificar que l'usuari es propietari dels arxius que son transferits.*
>>
>> *__gid__ -> Aixó ens permet definir el group del que serà propietari dels arxius que siguins transferits quan el servei sigui executat com a guest.*
>>
>> *__read only__ -> Determina si els clients que es conectan a rsync poden pujar arxius o no, el valor per default d'aquest paràmetre es true per tots els móduls.*
>>
>> *__list__ -> Permet al módul que sigui llistat quan els clients demanen una llista de módusl disponibles, posen __false__ amagan el módul del llistat.*
>>
>> *__auth users__ -> És un llistat dels usuaris que tenen permís per accedir al contingut d'aquest módul, els usuraris separats per comas. Els usuaris no necessitan existir en el sistema, són definits per l'arxiu secrets.*
>>
>> *__secrets file__ -> Defineix l'arxiu que conté els noms d'usuaris i les passwords dels usuaris vàlids de rsync.*
>>
>> *__hosts allow__ -> Són les direccions que tener permís per conectarse al sistema. Sense aquest paràmetre qualsevol ordinador té permís per conectar-se.*

__A continuació, desde el client, farem el següent:__

>```
> $ rsync -ap 10.200.245.212::
> ```

>> *Activa el módul que volem compartir.*

> ```
> $ rsync -rtv <user_compartit>@<host/IP_SERVIDOR>::<nom_modul_compartit(epoptes --> fitxer > rsyncd.conf)>/<path_que_volem_compartir(*)> <path_on_volem_compartir-lo>(.)
>```



































