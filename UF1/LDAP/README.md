# LUCAS RODRÍGUEZ CABAÑEROS A190074LR

> *Per encendre un docker "buit".*

> ```
> docker run --rm -it debian
>  ```


# LDAP 22

## INSTALACIÓ D'UN SERVIDOR BASE LDAP


### 1. Instal·lar paquest ldap

```
# apt-get update
```
```
# apt-get install -y slapd ldap-utils
```

### 2. Borrar el contingut existent del directori de configuració.

```
# rm -rf /etc/ldap/slapd.d/*
```
> *Comprovar si s'ha esborrat correctament.*

> ```
> tree /etc/ldap/slapd.d
> ```

### 3. Borrar el  contingut existent del directori de dades existent.

```
# rm -rf /var/lib/ldap/*
```
> *Comprovar si s'ha esborrat correctament.*

> ```
> tree /var/lib/ldap
> ```

### 4. Construir el fitxer de configuració procedent de la web.

> *https://github.com/edtasixm06/ldap21/blob/master/ldap21:base/slapd.conf*

```
# vim slapd.conf
```
> 4.1 Generar la configuració dinàmica.

> ```
> # slaptest -f slapd.conf -F /etc/ldap/slapd.d
> ```

>> *Comprovar.*

>> ```
>> tree /etc/ldap/slapd.d
>> ```


> 4.2 Construir el fitxer d'informació de la BD.

> *https://github.com/edtasixm06/ldap21/blob/master/ldap21:base/organitzacio-edt.org.ldif*

> ```
> # vim edt-org.ldif
> ```
> 4.1 Afegir l'informació a la BD.

> ```
> # slapadd -F /etc/ldap/slapd.d/ -l edt-org.ldif
> ```

>> *Comprovar.*

>> ```
>> tree /var/lib/ldap/
>> ```

### 5. Cambiar el propietari.

```
# chown -R openldap.openldap /etc/ldap/slapd.d /var/lib/ldap
```

### 6. Engegar el dimoni.

```
# slapd
```

> 6.1 Comprovar l'estat del dimoni.

> ```
> # ps ax
> # nmap localhost
> # ldapsearch -x -LLL -b 'dc=edt,dc=org' dn
> ```

## DOCKERFILE PEL SERVIDOR LDAP

### 1. Creació del Dockerfile.

```
# ldapserver 2022

FROM debian:latest
LABEL author="@edt ASIX Curs 2022"
LABEL subject="ldapserver 2022"
ARG DEBIAN_FRONTEND=noninteractive
RUN apt-get update
RUN apt-get install -y procps iproute2 iputils-ping nmap tree slapd ldap-utils
RUN mkdir /opt/docker/
WORKDIR /opt/docker/
COPY * /opt/docker/
RUN chmod +x /opt/docker/startup.sh
CMD /opt/docker/startup.sh
EXPOSE 389
```

> *· ARG DEBIAN_FRONTEND=noninteractive -> Definir una variable d'entorn dins d'un docker file, per tal de fer l'instalació dels paquets slapd no interactius.*

> *· RUN chmod +x /opt/docker/startup.sh -> Assignar els permissos correctes al script.*

> *· EXPOSE 389 -> EL port que farà servir el container.*

### 4. Creació del script.

```
#! /bin/bash

echo "Inicialització BD ldap edt.org"

rm -rf /etc/ldap/slapd.d/*
rm -rf /var/lib/ldap/*
slaptest -f slapd.conf -F /etc/ldap/slapd.d
slapadd -F /etc/ldap/slapd.d/ -l edt-org.ldif
chown -R openldap.openldap /etc/ldap/slapd.d /var/lib/ldap
/usr/sbin/slapd -d0
```

> * · /usr/sbin/slapd -d0 -> Per quan el script s'acabi el container no és mori.*

### 3. Construcció de l'imatge.

```
$ docker build -t a190074lr/ldap22:base .
```

### 4. Encendre el container.

```
$ docker run --name ldap22 -h ldap22 --net 2hisx -p 389:389 -d a190074lr/ldap22:base
```