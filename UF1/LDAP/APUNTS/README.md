# LUCAS RODRÍGUEZ CABAÑEROS A190074LR

> *Els exercicis es farán contra la network 2hisx -> 172.17.0.0/16.*

> *Ip container -> 172.17.0.2.*

> *Per crear la network.*

> ```
> $ docker network create 2hisx
>  ```

> *Encedre el meu docker ldap22:base.*

> ```
> $ docker run --rm --name ldap.edt.org -h ldap.edt.org --net 2hisx -p389:389 -d a190074lr/ldap22:base
>  ```

>> *-p: Propagació de ports.*

>> *-d: detach -> En segon pla.*

> *cn: Pere Pou*

> ```
> cn -> Common Name -> Atribut.
> Pere Pau: Valor.
>  ```

# APUNTS LDAP 22

## LDAPSEARCH

__Serveix per fer una recerca a la BD.__

### 1. OPCIONS


> __-x:__ *No métode autenticació (user/passwd).*

> __-LLL:__ *Format xerraire del llistat ( -L -LL -LLL cap ).*

> __-h:__ *Contra quin ordinador és fa la consulta.*

>>  *· Si és localhost és pot obviar.*

>>	*· Contra el container -> 172.17.0.2:389*

> __-b:__ *Base search A quina base de dades ha de buscar.*

>>	*· 'dc=edt,dc=org'*

>>	*· dn: Distinguished Name.*

> __-w:__ *Per dir-li la contrasenya de l'administrador, per línea de comandes.*

> __-W:__ *Per preguntar la constrasenya.*

> __-f:__ *Per llegir el fitxer d'entrada standard.*

> __-D:__ *Executar l'ordre en nom d'un usuari ( si l'usuari porta espai ha d'anar entre cometes, sino no ).*

> __-r:__ *Per fer una eliminació recursiva.*

> __-s:__ *SCOPE/ABAST.*

> __-v:__ *Verbose, per veure que esta fent.*

> __-u:__ *Verificar si es pot fer l'ordre de slaptest _PERO SENSE FER-LA_.*

### 2. EXEMPLES

__Posar filtres(Where):__ *Llistem tots els que siguin de la família pou ( sigui quin sigui el nom ).*

> __Del usuari pere pou mostrar; login, uid y el gid.__

> ```
> $ ldapsearch -x -LLL -b 'dc=edt,dc=org' 'cn=pere pou' dn uid uidnumber gidnumber
> ```

> __De qualsevol usuari que és digui pou.__

> ```
> $ ldapsearch -x -LLL -b 'dc=edt,dc=org' 'cn=*pou' dn uid uidnumber gidnumber
> ```

__Apendre a utilitzar scope.__

> __+:__ *Mostra els atributs FUNCIONALS que posa el motor de la BD.*

> ```
> $ ldapsearch -x -LLL -b 'dc=edt,dc=org' 'cn=pere pou' +
> ``` 

> __Tots aquells amb gidNumber=600.__

> ```
> $ ldapsearch -x -LLL -b 'dc=edt,dc=org' 'gidNumber=600' 
> ``` 
  
> __Mostrar tots els usuaris que és diguin pou o aquells que tenen el gidNumber=600.__

> ```
> $ ldapsearch -x -LLL -b 'dc=edt,dc=org' '(|(gidNumber=600) (cn=*pou))' dn gidNumber
> ``` 
  
> __Mostrar el gidNumber i el dn de tots aquells que és diuen Mas I amb el gidNumber=600.__

> ```
> $ ldapsearch -x -LLL -b 'dc=edt,dc=org' '(&(gidNumber=600) (cn=* Mas))' dn gidNumber
> ``` 
  
> __Mostrar el gidNumber i el dn de tots aquells que és diuen MAS I amb el gidNumber=600 O que Diuen Jordi i algu més.__

> ```
> $ ldapsearch -x -LLL -b 'dc=edt,dc=org' '(|(&(gidNumber=600)(cn=* Mas)) (cn=Jordi *) )' dn gidNumber
> ``` 
  
> __BASE__

> __ONE:__ *One level inferior, pels fills de la base ( sense mostrar la base ).*
		
> __CHILDREN:__ *Tots els nivells inferiors, menys el que preguntas.*
		
> __SUB:__ *Tots els nivells des de on preguntas.*

> ```
> $ ldapsearch -x -LLL -b 'dc=edt,dc=org' -s base
> ``` 
  
> __Busquem informació del uid corresponent A LA BD de l'escola del treball.__

> ```
> $ ldapsearch -x -LLL -h ldap.escoladeltreball.org -b 'dc=escoladeltreball,dc=org' 'uid=a190074lr'
> ``` 
  
## ORDRES CLIENTS

__No les fem des de el servidor sino des de el client.__
 
__L'Administrador de la BD i l'unic que pot fer modificacions és diu Manager.__

> *· NO existeix com usuari linux.*
> 
> *· NO existeix com usuari de la BD.*

### 1. LDAPADD

__Serveix per afegir registres a la BD.__

__S'ha de treballar i executar les ordres com a propietari de la BD o tenint permissos de la BD.__


#### EXEMPLE

> __-x:__ *No métode autenticació (user/passwd).*
> 
> __-D:__ *Alhora d'afegir ens hem d'identificar com a Manager.*

> __-w:__ *Introduïr a la línea de comandes la contrasenya de Manager.*

> __-f:__ *Afegir els continguts segons l'informació dels fitxers.*

>> __-c:__ *Continua encara que hi hagin errors.*

> ```
> $ ldapadd -x -D 'cn=Manager,dc=edt,dc=org' -w secret -f /tmp/ldap/usuaris1.ldif
> ```

### 2. LDAPDELETE

__Serveix per elimar registres a la BD.__

__S'ha de treballar i executar les ordres com a propietari de la BD o tenint permissos de la BD.__

#### EXEMPLE

> __-x:__ *No métode autenticació (user/passwd).*

> __-D:__ *Alhora d'afegir ens hem d'identificar com a Manager.*

> __-w:__ *Introduïr a la línea de comandes la contrasenya de Manager.*
 
>> __-r:__ *Borrar recursivament.*

> ```
> $ ldapdelete -x -D 'cn=Manager,dc=edt,dc=org' -w secret 'cn=Pau Pou,ou=usuaris,dc=edt,dc=org'
> ```

### 3. LDAPMODIFY

__Serveix per modificar registres a la BD.__

__CADASCÚN POT MODIFICAR LO SEU I VEURE LA RESTRE, menys Manager que pot modificar tot, és a dir;.__

> *· L'usuari Anna no pot modificar coses de l'usuari Pere.*

#### EXEMPLE

> __· mod.ldif__

> ```
> dn: cn=Pere Pou,ou=usuaris,dc=edt,dc=org
> changetype: modify
> replace: mail
> mail: patimpatam@edt.org
> ```

> __-v:__ *Mostrar el procés per pantalla.*

> __-x:__ *No métode autenticació (user/passwd).*

> __-D:__ *Alhora d'afegir ens hem d'identificar com a Manager.*

> __-w:__ *Introduïr a la línea de comandes la contrasenya de Manager.*

> __-f:__ *Modificar els continguts segons l'informació dels fitxers.*

> ```
> $ ldapmodify -vx -D 'cn=Pere Pou,ou=usuaris,dc=edt,dc=org' -w pere -f mod.ldif
> ```

### 4. LDAPMODRDN

__Serveix per modificar el relative distinguished name.__

__S'ha de treballar i executar les ordres com a propietari de la BD o tenint permissos de la BD.__

#### EXEMPLE

> __· Escribim la "ruta absoluta" del que volem cambiar i per la que la volem cambiar.__

>> __-x:__ *No métode autenticació (user/passwd).*

>> __-D:__ *Alhora d'afegir ens hem d'identificar com a Manager.*

>> __-w:__ *Introduïr a la línea de comandes la contrasenya de Manager.*

>> __-r:__ *Es carregà tots el rdn anteriors.*

> ```
> $ ldapmodrdn -x -D 'cn=Manager,dc=edt,dc=org' -w secret 'cn=Anna Pou,ou=usuaris,dc=edt,dc=org' 'cn=AnnitaPuig'
> ```

### 5. LDAPCOMPARE

__Validar/Verifica si existeix el valor del camp introduït.__

#### EXEMPLE

> __TRUE__

> ```
> $ ldapcompare -x 'cn=Pere Pou,ou=usuaris,dc=edt,dc=org' mail:pere@edt.org
> ```

> FALSE

> ```
> $ ldapcompare -x 'cn=Pere Pou,ou=usuaris,dc=edt,dc=org' mail:batman@edt.org
> ```


### 6. LDAPWHOAMI

__Per observar la "ruta absoluta" d'un usuari.__

#### EXEMPLE

> __-x:__ *Anonymus*

> ```
> $ ldapwhoami -x
> ```

>> ```
>> anonymous
>> ```

> __-x:__ *No métode autenticació (user/passwd).*

> __-D:__ *Alhora d'afegir ens hem d'identificar com a Manager.*

> __-W:__ *Per preguntar la constrasenya.*

> ```
> $ ldapwhoami -x -D 'cn=Pere Pou,ou=usuaris,dc=edt,dc=org' -W
> ```

>> ```
>> Enter LDAP Password: pere
>> dn:cn=Pere Pou,ou=usuaris,dc=edt,dc=org
>> ```

### 7. LDAPPASSWD

__Per cambiar-ne el passwd d'un usuari de la BD.__

__CADASCÚN POT MODIFICAR LO SEU I VEURE LA RESTRE, menys Manager que pot modificar tot.__

#### EXEMPLE

> __Assigna de forma aleatoria y automàtica una nova contraseña, i ens enseña quina es__

> ```
> $ ldappasswd -vx -D 'cn=Pere Pou,ou=usuaris,dc=edt,dc=org' -w pere
> ```

> __-S:__ *Per demanar serà la nova contraseña.*

> ```
> $ ldappasswd -vx -D 'cn=Pere Pou,ou=usuaris,dc=edt,dc=org' -w IXve8Wmd -S
> ```

> __-s:__ *Introduïm a la línea de comandes la nova contraseña directament.*

> ```
> $ ldappasswd -vx -D 'cn=Pere Pou,ou=usuaris,dc=edt,dc=org' -w jupiter -s pere
> ```

### 8. GETENT

__Consultar la configuració actual del sistema, dels usuaris i groups.__

#### EXEMPLE

> __Assigna de forma aleatoria y automàtica una nova contraseña, i ens enseña quina es__

> ```
> $ $ getent passwd a190074lr
> ```

> ```
> $ getent group docker
> ```

## ORDRES SERVIDOR

__Per executar les ordres dins del container.__
 
__L'Administrador de la BD i l'unic que pot fer modificacions és diu Manager.__

> __-it:__ *Interactiu.*
 
> __/bin/bash:__ *Per entrar a la terminal del container.*

> __Aquestes ordres no requereixen que el servidor estigui activat, sino que actuant contra el backend.__

> __Alguna d'elles causarant  problemes si les usem amb el servidor engegat (com splatest,slapadd...). __

### 1. SLAPPASSWD

__Per veure com sería un passwd amb el format demanat.__

#### EXEMPLE

>  __El per defecte SSHA.__

> ```
> # slappasswd
>		
>		* New password: 
>		  Re-enter new password: 
>		  {SSHA}xPJgvq510mJd+zFX7K+uAgD7bqlrzYhR
> ```

>  __Formar MD5.__

> ```
> # slappasswd -h {md5}
>		
>		* New password: 
>		  Re-enter new password: 
>		  {MD5}J6UUjqD73a4i2QK+qaGVMQ==
> ```

>  __Formar CRYPT.__

> ```
> # slappasswd -h {CRYPT}
>		
>		* New password: 
>		  Re-enter new password: 
>		  {CRYPT}JpaDCMEkNCNY.
> ```

> __Si fessim un modify amb un replace del passwd, es veuría la contraseña en text pla, sense cap xifrat en concret. Pero alhora de veure la nova contraseña en el document edt-org.ldif la veuriem en codi 64bits.__

>> __Per transformar un arxiu en base 64 -> 256 caràcters del codi ASCI només utilitze 64 caràcters.__

>> ```
>> $ base64 [DOCNAME]
>> ```

### 2. SLAPCAT

__Fa un volcat de les dades de la BD.__

#### EXEMPLE

>  __Fa un volcat del propi dimoni -> cn:config.__

> ```
> $ docker exec -it ldap22_prova slapcat -n 0
> ```

>  __Fa un volcat contra la propi BD.__

> ```
> $ docker exec -it ldap22_prova slapcat -n 1
> ```

## CONFIGURACIÓ DINÀMICA DEL SERVIDOR

__Docker Eduard.__

> ```
> docker run --rm --name ldap.edt.org -h ldap.edt.org --net 2hisx -p 389:389 -d edtasixm06/ldap22:config
> ```

> __Introduïr la següent informació al fitxer slapd.conf, per tal de poder editar la configuració del dimoni en "calent", és a dir sense haver-hi d'aturar el dimoni.__

> ```
> database config
> rootdn "cn=Sysadmin,cn=config"
> rootpw {SSHA}vwpQxtzc7yLsGg8K7fm02p2Fhox/PFP4
> ```

> __Per observar la BD config, peró ens hem d'autentificar com l'administrador d'aquesta BD.__

> ```
> $ ldapsearch -x -LLL -D 'cn=Sysadmin,cn=config' -w syskey -b 'cn=config' dn
> ```

>> *És el mateix que fet slapcat -n0.*

> __Per observar en concret un "camp" d'aquesta BD.__

> ```
> $ ldapsearch -x -LLL -D 'cn=Sysadmin,cn=config' -w syskey -b 'olcDatabase={0}config,cn=config'
> ```

> __Modificar el fitxer de configuració en calent.__

> ```
> $ ldapmodify -vx -D 'cn=Sysadmin,cn=config' -w syskey -f mod3.ldif 
> ```

## OPERADORS

__· ( OPERADOR ( PRIMERA CONDICIÓ ) ( CONDICIÓ 2) )__

> * | -> OR*

> * & -> AND*
## PERMISSOS

### STANDART

__Permissos de que tothom pot LLEGIR i EDITAR lo seu, pero només pot LLEGIR la resta NO EDITAR.__

```
access to * by self write by * read
```

__Permissos de que tothom pot LLEGIR i EDITAR lo seu i la RESTA.__

```
access to * by * write
```
#### EXEMPLES

> __L’usuari “Anna Pou” és ajudant de l’administrador i té permisos per modificar-ho tot. Tothom pot veure totes les dades de tothom.__

> ```
> acces to * by dn.exact"uid=anna,ou=usuaris,dc=edt,dc=org" write
> acces to * by * read
> ```

> __L’usuari “Anna Pou” és ajudant d’administració. Tothom es pot modificar el seu propi email i homePhone. Tothom pot veure totes les dades de tothom.__

> ```
> access to attrs=mail,homePhone
>		by dn.exact="ui=anna,.." write 
>		by self write
>		by * read 
> ```

> ```
> acces to * (para el resto de campos) 
>       by dn.exact="ui=anna,ou=usuaris,dc=edt,dc=org" write
>		by * read
> ```

> __Tot usuari es pot modificar el seu mail. Tothom pot veure totes les dades de tothom.__

> ```
> (olcAccess)	access to attrs=mail 
>			by self write
>			by * read 
>		
>			access to * by * read
>			[ access to * by * none] 
> ```

> __Tothom pot veure totes les dades de tothom, excepte els mail dels altres.__

> ```
> mail
>		access to attrs=mail 
>			by self read
>			[by * none] 
>
>		access to * by * read 
> ```

> __Tot usuari es pot modificar el seu propi password i tothom pot veure totes les dades de tothom.__

> ```
> olcAccess to attrs=userPassword
>			 by self write
>			 by * read (search, compare) 
>	       	  olcAccess to * by * read 
> ```

> __Tot usuari es pot modificar el seu propi password i tothom pot veure totes les dades de tothom, excepte els altres passwords.__

> ```
> acces to attrs=userPassword
>		by self write 
>		by * auth
>		[by none]
> access to * by * read
> ```

> __Tot usuari es pot modificar el seu propi password i tot usuari només pot veure les seves pròpies dades.__

> ```
> access to attrs=userPassword
>		by self write
>		by * auth
> access to * by self read 
>		[by * none ]
> ```

> __Tot usuari pot observar les seves pròpies dades i modificar el seu propi password, email i homephone. L’usuari “Anna Pou” pot modificar tots els atributs de tots excepte els passwords, que tampoc pot veure. L’usuari “Pere Pou” pot modificar els passwords de tothom.__

> ```
> acces to attrs=userPassword 
>		by dn.exact"uid=Pere,ou=usuaris,dc=edt,dc=org" write 
>		by self write
>		by * auth
> ```

> ```
> access to attrs=email,homePhone
>		by dn.exact"uid=anna,ou=usuaris,dc=edt,dc=org" write 
>		by self write
>		[by * none]
> ```

> ```
> access to * 
>		by dn.exact"uid=anna,ou=usuaris,dc=edt,dc=org" write 
>		by self read 
>		by * search
> ```

> __Els usuaris poden modificar passwd, mail i phone no pueden ver el passwd ni el phone de altres las resta de atributos los puede ver todos.__
  
> ```
> passwd 
> mail 
> homepohne 
> 
>		access to attrs=userPassword
>				by self write
>				by * auth 
>				[by * none]	 
>
>			access to attrs=mail
>				by self write
>				by * read 
>		
>			acces to attrs=homePhone
>				by self write
>				[by * none]	 
>
>			access to * 
>				by * read 
> ```

> __Anna puede modifica todos los campos pere es ayudante de ana, puede modificar el mail y phone todos puedes ver el resto de atributos de todos menos el password.__
  
> ```
> acces to attrs=userPassword
>		by dn.exact"uid=anna Pou,ou=usuaris,dc=edt,dc=org" write 
>		by * auth ( tiene dercho a acceder al campo passwd solo para utenticarse)
> ```

> ```
> acces to attrs=mail,homePhone
>		by dn.exact"uid=anna,ou=usuaris,dc=edt,dc=org" write 
>		by dn.exact"uid=pere,ou=usuaris,dc=edt,dc=org" write
>		by * read 
> ```

> ```
> acces  to * 
>		by dn.exact"uid=anna,ou=usuaris,dc=edt,dc=org" write 
>		by * search
> ```

> __Anna es admin i puede modificar todo de todos  pere sigue siendo ayudante  y modificar el phone i mail los usuaris puede ver el resto de atributos menos el homephone i passwd, solo puede ver el suyo.__
  
> ```
> acces to attrs=userPassword
>		by dn.exact"uid=anna,ou=usuaris,dc=edt,dc=org" write
>		by self read  
>		by * auth ( tiene dercho a acceder al campo passwd solo para utenticarse) 
> ```

> ```
> acces to attrs=mail
>		by dn.exact"uid=anna,ou=usuaris,dc=edt,dc=org" write 
>		by dn.exact"uid=pere,ou=usuaris,dc=edt,dc=org" write
>		by * read 
> ```

> ```
> acces to attrs=homePhone
>		by dn.exact"uid=anna,ou=usuaris,dc=edt,dc=org" write 
>		by dn.exact"uid=pere,ou=usuaris,dc=edt,dc=org" write
>		by self read 
>		[by * none]
> acces  to * 
>		by dn.exact"uid=anna,ou=usuaris,dc=edt,dc=org" write 
>		by * search
> ```

## SCHEMA

__Encendre el PHPLDAPADMIN.__

```
$ docker run --rm --name phpldapadmin -h phpldapadmin --net 2hisx -p 80:80 -d edtasixm06/phpldapamin
```

> *Entra al navegador -> localhost/phpldapadmin*

> __AUXILIARY:__ *Si no es defineix el default és __STRUCTURAL.__*