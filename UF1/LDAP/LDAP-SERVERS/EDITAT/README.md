# Lucas Rodríguez Cabañeros

# LDAP22 EDITAT

## Generar un sol fitxer ldif anomenat edt-org.ldif (usuaris0-11).

Afegir al fitxer *edt-org-ldif* els usuaris del 01-11 del repository de github: __*https://github.com/edtasixm06/ldap21/blob/master/ldap21:base/usuaris-mes-edt.org.ldif*__

## Afegir en el fitxer dos usuaris i una ou nova inventada i posar-los dins la nova ou.

```
$ vim edt-org.ldif
```

```
dn: ou=becaris,dc=edt,dc=org
ou: becaris
description: Container per a maquines linux
objectclass: organizationalunit
```

Per encriptar la passwd del nous usuaris utilizem slappaswd ( dins d'un docker que tingui el servidor instal·lat ).

```
root@ldap22:/opt/docker# slappasswd
	
	lucas -> {SSHA}XKJg6L7CZ6MfQkeocTT0A3TiJcdt1vL8
	marc -> {SSHA}oUtJ4OfbIexgBM+uui57IDKlqvxHqZem
```

```
dn: uid=Lucas,ou=becaris,dc=edt,dc=org
objectclass: posixAccount
objectclass: inetOrgPerson
cn: Lucas
cn: Lucas el becari
sn: BECARI RODRI
homephone: 555-444-3333
mail: lucas@edt.org
description: Becaris de ASIX
ou: becaris
uid: Lucas
uidNumber: 7777
gidNumber: 444
homeDirectory: /tmp/home/becaris/Lucas
#USERPASSWD -> lucas
userPassword: {SSHA}XKJg6L7CZ6MfQkeocTT0A3TiJcdt1vL8 
```

```
dn: uid=Marc,ou=becaris,dc=edt,dc=org
objectclass: posixAccount
objectclass: inetOrgPerson
cn: Marc
cn: Marc el becari
sn: BECARI PORTO
homephone: 555-333-444
mail: lucas@edt.org
description: Becaris de ASIX
ou: becaris
uid: Marc
uidNumber: 6666
gidNumber: 333
homeDirectory: /tmp/home/becaris/Marc
#USERPASSWD -> marc
userPassword: {SSHA}oUtJ4OfbIexgBM+uui57IDKlqvxHqZem
```

## Modificar el fitxer edt.org.ldif  modificant dn dels usuaris utilitzant en lloc del cn el uid per identificar-los (rdn).

```
$ vim edt.org.ldif
```

```
dn: cn=pau,ou=usuaris,dc=edt,dc=org -> dn: uid=pau,ou=usuaris,dc=edt,dc=org
```

## Configurar el password de Manager que sigui ‘secret’ però encriptat (posar-hi un comentari per indicar quin és de cara a estudiar).

Editar el fitxer slapd.conf i encriptar la passwd amb slappaswd:

```
root@ldap22:/opt/docker# slappasswd

	secret -> {SSHA}IxvlrMn2N6PIejN3H06UP/6k+exxELLy
```

```
$ vim slapd.conf
```

```
database mdb
suffix "dc=edt,dc=org"
rootdn "cn=Manager,dc=edt,dc=org"
# ROOTPW -> secret
rootpw {SSHA}IxvlrMn2N6PIejN3H06UP/6k+exxELLy
directory /var/lib/ldap
index objectClass eq,pres
access to * by self write by * read
```

## Propagar el port amb -p -P.

Deixar el port obert en el Dockerfile:

```
EXPOSE 389
```

Alhora de fer el docker run especificar __-p 389:389__ per propogar el port:

```
$ docker run --rm --name ldap.edt.org -h ldap.edt.org --net 2hisx __-p 389:389__ -d a190074lr/ldap22:editat
```

## Configurar la base de dades cn=config amb un usuari administrador anomenat syadmin i password syskey.

Encriptar la passwd amb slappasswd:

```
$ vim slapd.conf
```

```
root@ldap22:/opt/docker# slappasswd

	syskey -> {SSHA}IxvlrMn2N6PIejN3H06UP/6k+exxELLy
```

```
database config
rootdn "cn=Sysadmin,cn=config"
# ROOPASWD -> syskey
rootpw {SSHA}vwpQxtzc7yLsGg8K7fm02p2Fhox/PFP4
```
