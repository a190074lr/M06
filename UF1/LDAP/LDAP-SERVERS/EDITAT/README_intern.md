# Lucas Rodríguez Cabañeros

# LDAP22 EDITAT

## Descriure els canvis i les ordres per posar-lo en marxa.

Construir l'imatge.

```
$ docker build -t a190074lr/ldap22:editat .
```

Encendre el container.

```
$ docker run --rm --name ldap.edt.org -h ldap.edt.org --net 2hisx -p 389:389 -d a190074lr/ldap22:editat
```

## Verifiqueu que sou capaços de modificar la configuració de la base de dades edt.org en calent modificant per exemple el passwd o el nom o els permisos.

Per observar la BD config, peró ens hem d'autentificar com l'administrador d'aquesta BD ( -> És el mateix que fet slapcat -n0 ). 

```
$ ldapsearch -x -LLL -D 'cn=Sysadmin,cn=config' -w syskey -b 'cn=config' dn 
```

Per observar en concret un apart del fitxer de configuració d'aquesta BD.

```
$ ldapsearch -x -LLL -D 'cn=Sysadmin,cn=config' -w syskey -b 'olcDatabase={0}config,cn=config'
```

```
dn: olcDatabase={1}mdb,cn=config
objectClass: olcDatabaseConfig
objectClass: olcMdbConfig
olcDatabase: {1}mdb
olcDbDirectory: /var/lib/ldap
olcSuffix: dc=edt,dc=org
olcAccess: {0}to *  by self write  by * read
olcAddContentAcl: FALSE
olcLastMod: TRUE
olcMaxDerefDepth: 15
olcReadOnly: FALSE
olcRootDN: cn=Manager,dc=edt,dc=org
olcRootPW: {SSHA}IxvlrMn2N6PIejN3H06UP/6k+exxELLy
olcSyncUseSubentry: FALSE
olcMonitoring: TRUE
olcDbNoSync: FALSE
olcDbIndex: objectClass pres,eq
olcDbMaxReaders: 0
olcDbMaxSize: 10485760
olcDbMode: 0600
olcDbSearchStack: 16
olcDbRtxnSize: 10000
```

Modificar el fitxer de configuració en calent.

```
$ cat mod.ldif
```

```
# Atenció no deixar espais als finals de linia
dn: olcDatabase={1}mdb,cn=config
changetype: modify
replace: olcRootPW
olcRootPw: secret
-
delete: olcAccess
-
add: olcAccess
olcAccess: to * by * read
```

```
$ ldapmodify -x -D 'cn=Sysadmin,cn=config' -w syskey -f mod.ldif
```

Comprovar que s'ha pogut editar el fixer en configuració calent.

```
$ ldapsearch -x -LLL -D 'cn=Sysadmin,cn=config' -w syskey -b 'olcDatabase={1}mdb,cn=config'

dn: olcDatabase={1}mdb,cn=config
objectClass: olcDatabaseConfig
objectClass: olcMdbConfig
olcDatabase: {1}mdb
olcDbDirectory: /var/lib/ldap
olcSuffix: dc=edt,dc=org
olcAddContentAcl: FALSE
olcLastMod: TRUE
olcMaxDerefDepth: 15
olcReadOnly: FALSE
olcRootDN: cn=Manager,dc=edt,dc=org
olcSyncUseSubentry: FALSE
olcMonitoring: TRUE
olcDbNoSync: FALSE
olcDbIndex: objectClass pres,eq
olcDbMaxReaders: 0
olcDbMaxSize: 10485760
olcDbMode: 0600
olcDbSearchStack: 16
olcDbRtxnSize: 10000
olcRootPW: secret
olcAccess: {0}to * by * read
```
