# ldap 2022
## @edt M06-ASIX

#### Ldapserver 2022

 * **ldap22:base** servidor ldap bàsic, instal·la base de dades *edt.org.*

```
$ docker build -t lucas/ldap22:base .
```

```
$ docker run --rm --name ldap.edt.org -h ldap.edt.org -it lucas/ldap22:base
```
